require("dotenv").config({ path: ".env" });

const cloudinary = require('cloudinary').v2;


cloudinary.config({
    cloud_name: process.env.CLOUD_NAME_CLOUDINARY,
    api_key: process.env.API_KEY_CLOUDINARY,
    api_secret: process.env.API_SECRET_CLOUDINARY
});

const cloudinaryImageUploadMethod = file => {
    return new Promise(resolve => {
        cloudinary.uploader.upload(file, (err, res) => {
            if (err) console.log("upload image error: ",err)
            resolve({
                url: res.url
            })
        })
    })
}

module.exports = { cloudinary, cloudinaryImageUploadMethod }