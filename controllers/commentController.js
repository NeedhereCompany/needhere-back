const { validationResult } = require("express-validator");
const Comment = require("../models/Comment");


//* CREATE COMMENT *//
exports.createComment = async (req, res) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    if (!req.user) {
      return res.status(400).json({ message: 'Unauthorized user'});
    }
    
    const comment = new Comment(req.body);
    comment.userId = req.user.id;
    comment.username = req.user.name;
    comment.pic = req.user.pic ? req.user.pic : '';

    comment.save();
    
    res.json(comment);
  } catch (error) {
    console.log(error);
    res.status(500).send("Error exists on server.");
  }
};

//* GET COMMENTS BY POST *//
exports.findAllByPostComments = async (req, res) => {
  const { postId } = req.params;
  try {
    const comments = await Comment.find({ postId });
    const data = {
      length: comments.length,
      comments
    }
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).send("Error exists on server.");
  }
};

//* GET AN COMMENT *//
exports.findCommentById = async (req, res) => {
  const { id } = req.params;
  try {
    const comment = await Comment.findById(id);
    
    res.json(comment);
  } catch (error) {
    console.log(error);
    res.status(500).send("Error exists on server.");
  }
};

//* UPDATE A COMMENT *//
exports.updateComment = async (req, res) => {
  try {
    
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { description } = req.body;
    const { id } = req.params;

    const newComment = {};

    if (description) {
      newComment.description = description;
    }

    let commentExists = Comment.findById(id);

    if (!commentExists) {
      return res.status(404).json({ message: "comment does't exists" });
    }

    commentExists = await Comment.findOneAndUpdate(
      { _id: id },
      { $set: newComment },
      { new: true }
    );

    return res.status(201).json(commentExists);

  } catch (error) {
    res.status(500).send("Error exists on server.");
  }
};

//* DELETE A COMMENT *//
exports.deleteComment = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { user } = req;

    let commentExists = await Comment.findById(id);
    if (!commentExists) {
      return res.status(404).json({ message: "Comment does'nt exists!" });
    }

    if (commentExists.userId.toString() !== user.id) {
      return res.status(401).json({ message: `Unauthorized User ${user.id}` });
    }
    await Comment.findOneAndRemove({ _id: id });

    res.status(201).json({ message: "Deleted successful" });
  } catch (error) {
    console.log(error);
    res.status(500).send("Error exists on server");
  }
};
