const jwt = require("jsonwebtoken");
const bcryptjs = require("bcryptjs");
const { validationResult } = require("express-validator");
const User = require("../models/User");
const { cloudinary } = require('../providers/CloudinaryService');


//* UPDATE A USER *//
exports.updateUser = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  let pic = "";
  const { id } = req.params;
  const { name, username, phone } = req.body;


  try {


    let userExists  = await User.findById(id);

    if (!userExists) {
      return res.status(404).json({ message: "User doesn't exists" });
    }

    if (req.files) {
      const file = req.files.pic;
  
      await cloudinary.uploader.upload(file.tempFilePath, (err, result) => {
        if (err) {
          return res.status(404).json({ message: "Error uploading: "+err });
        }
        pic += result.url;
      });
    }
    const newPost = {
      name, username, phone, pic
    }
    const user = await User.findOneAndUpdate(
      { _id: id },
      { $set: newPost },
      { new: true }
    );

    res.json({ user });

  } catch (error) {
    
    res.status(500).send("Error exists on server: "+error);
  }
};

//* GET ALL USER *//
exports.findAllUsers = async (req, res) => {
  try {
    const users = await User.find();
    const data = {
      length: users.length,
      users
    }
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).send("Error exists on server.");
  }
};

//* GET ONE USER *//
exports.findOneUser = async (req, res) => {
  try {
    const {id} = req.params;
    const user = await User.findById(id);
    
    res.json(user);

  } catch (error) {
    console.log(error);
    res.status(500).send("Error exists on server.");
  }
};

//* FOLLOW A USER *//
exports.followUser = async (req, res) => {
  try {

    const { user }    = req;
    const { userId }  = req.params;

    //*  Whom Followed *//
    await User.findByIdAndUpdate(
      { _id: user.id },
      { $push: { following: userId } }
    );

    //* Whom Following *//
    await User.findByIdAndUpdate(
      { _id: userId },
      { $push: { followers: user.id } }
    );

    return res.status(200).send({ message: "User Follow Success" });
  } catch (e) {
    return res
      .status(500)
      .send({ message: "User Follow Failed", data: e.message });
  }
};

//* UNFOLLOW A USER *//
exports.unfollowUser = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  try {

    const { user }    = req;
    const { userId }  = req.params;

    await User.findByIdAndUpdate(
      { _id: user.id},
      { $pull: { following: userId } }
    );

    await User.findByIdAndUpdate(
      { _id: userId },
      { $pull: { followers: user.id } }
    );
    return res.status(200).send({ message: "User UnFollow Success" });
  } catch (e) {
    return res
      .status(500)
      .send({ message: "User UnFollow Failed", data: e.message });
  }
};
