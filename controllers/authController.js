const User = require("../models/User");
const bcryptjs = require("bcryptjs");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const formData = require("form-data");
const Mailgun = require("mailgun.js");
const mailgun = new Mailgun(formData);
const DOMAIN = "sandbox00efdcde5a904869973e1cef32efbee7.mailgun.org";
const mg = mailgun.client({
  key: process.env.MAILGUN_APIKEY,
  username: DOMAIN,
});

//* CREATE NEW USER - USER REGISTER *//
exports.registerAuth = async (req, res) => {

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { username, password, email, name,phone } = req.body;

  try {

    const userExists = await User.findOne({ email });

    if (userExists) {
      return res.status(400).json({ message: "User already exists" });
    }

    const user = new User({
      username, email, password, name,phone
    });

    const salt = await bcryptjs.genSalt(10);
    user.password = await bcryptjs.hash(password, salt);    
    await user.save();


    const payload = {
      user: { id: user.id },
    };

    const userSaved = await User.findById(user.id).select("-password");

    jwt.sign(
      payload,
      process.env.SECRET,
      {},
      (error, token) => {
        if (error) throw error;

        res.json({ token: token, user: userSaved });
      }
    );
  } catch (error) {

    res.status(500).send("Error exists on server: "+error);
  }
};

//*  AUTH - LOGIN *//
exports.loginAuth = async (req, res, next) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { username, password } = req.body;

  try {

    let userExists = await User.findOne({ username });

    if (!userExists) {
      return res.status(400).json({ message: "User does'nt exists" });
    }

    const correctPass = await bcryptjs.compare(password, userExists.password);

    if (!correctPass) {
      return res.status(400).json({ message: "Email or password invalid." });
    }

    
    const userSaved = await User.findById(userExists.id).select("-password");
    const payload = { user: { id: userExists.id } };

    jwt.sign(
      payload,
      process.env.SECRET,
      {/* Token never expire */ },
      (error, token) => {
        if (error) throw error;

        res.json({ token: token, user: userSaved });
      }
    );
  } catch (error) {
    console.log(error);
  }
};

//* FORGOT PASSWORD A USER *//
exports.forgotPasswordAuth = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  const { email } = req.body;

  let userExists = await User.findOne({ email });

  if (!userExists) {
    return res.status(400).json({ message: "User does'nt exists" });
  }
  const payload = { user: { id: userExists.id } };
  const token = jwt.sign(payload, process.env.RESET_PASSWORD_KEY, {
    expiresIn: 3600, 
  });

  const data = {
    from: "needhere@gmail.com",
    to: email,
    subject: "Account Activation Link",
    html: `<h2> please click on given link to reset your password</h2>
    
    <p>${process.env.CLIENT_URL}/resetpassword/${token}</p>`,
  };

  return userExists.updateOne({ resetLink: token }, (error, success) => {
    if (error) {
      return res.status(400).json({ error: "reset password error" });
    } else {
      mg.messages().send(data, function (error, body) {
        if (error) {
          return res.json({ error: error.message });
        }
        return res.json({
          message: "E-mail has been sent kindly follow the intrurions",
        });
      });
    }
  });
};
