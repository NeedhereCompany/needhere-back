const { validationResult } = require("express-validator");
const FeedModel = require("../models/Feed");


//* GET FEEDS *//
exports.findAllByFeeds = async (req, res) => {
  const { user } = req;
  
  try {

    if (!user) {
      return res.status(401).json({ message: `Unauthorized User` });
    }

    const feeds = await FeedModel.find();
    const data = {
      length: feeds.length,
      feeds
    }
    res.json(data);
  } catch (error) {
    console.log(error);
    res.status(500).send("Error exists on server.");
  }
};


