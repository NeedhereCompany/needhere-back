const express = require("express");
const connectDB = require("./config/db");
const fileupload = require('express-fileupload');

const app = express();
//* Database connected *//
connectDB();

app.use(fileupload({
  useTempFiles: true
}));

app.use('/uploads', express.static('uploads'));
app.use(express.json({ extended: true }));


app.use("/api/users/", require("./routes/users"));
app.use("/api/auth/", require("./routes/auth"));
app.use("/api/posts/", require("./routes/posts"));
app.use("/api/comments/", require("./routes/comments"));


app.listen(process.env.PORT, () => {
  console.log(`Server running on port  ${process.env.PORT}`);
});
