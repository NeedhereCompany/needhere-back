const mongoose = require("mongoose");

const Feedschema = mongoose.Schema({
  _id: {
    type: String,
  },
  pic: {
    type: String,
    trim: true,
  },
  description: {
    type: String,
  },
  userName: {
    type: String,
  },
  picUser: {
    type: String,
  },
  created: {
    type: Date,
    default: Date.now(),
  },
});
module.exports = mongoose.model("Feed", Feedschema);
