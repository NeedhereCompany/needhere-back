const mongoose = require("mongoose");

const CommentSchema = mongoose.Schema({
  postId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Post",
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  pic: {
    type: String,
    trim: true,
  },
  username: {
    type: String,
  },
  description: {
    type: String,
  },
});

module.exports = mongoose.model("Comment", CommentSchema);
