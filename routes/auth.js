const express = require("express");
const router = express.Router();
const { check } = require("express-validator");
const authController = require("../controllers/authController");


router.post(
  "/register",
  [
    check("name", "Name is required").not().isEmpty(),
    check("email", "Email invalid").isEmail(),
    check("password", "Password must be at least 6 characters long").isLength({ min: 6,})
  ],
  authController.registerAuth
);

router.post(
  "/login",
  [
    check("username", "Enter a valid username").isLength({ min: 1 }),
    check("password", "Fill in the field password").isLength({ min: 1 })
  ],
  authController.loginAuth
);

router.put(
  "/forgot-password",
  check("email", "Please provide a valid email").isEmail(),
  authController.forgotPasswordAuth
);

module.exports = router;
