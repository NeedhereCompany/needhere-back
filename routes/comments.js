const express = require("express");
const { check } = require("express-validator");
const router = express.Router();
const commentController = require("../controllers/commentController");
const ensuredAuthenticated = require("../middleware/auth");



router.get("/:postId", 
    ensuredAuthenticated, 
    commentController.findAllByPostComments
);
router.get("/byId/:id", 
    ensuredAuthenticated, 
    commentController.findCommentById
);
router.post("/",
    ensuredAuthenticated,
    [check("description", "Description must be at least one character").isLength({min: 1,})],
    commentController.createComment
);

router.patch("/:id",
    ensuredAuthenticated,
    [check("description", "Description must be at least one character").isLength({min: 1,})], 
    commentController.updateComment
);

router.delete("/:id", 
    ensuredAuthenticated, 
    commentController.deleteComment
);

module.exports = router;
