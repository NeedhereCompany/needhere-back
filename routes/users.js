const express = require("express");
const { check } = require("express-validator");
const router = express.Router();
const userController = require("../controllers/userController");
const ensuredAuthenticated = require("../middleware/auth");


router.patch("/:id", ensuredAuthenticated, userController.updateUser);
router.get("/", userController.findAllUsers);
router.get("/:id",userController.findOneUser);
router.patch("/follow/:userId", ensuredAuthenticated, userController.followUser);
router.patch("/unfollow/:userId", ensuredAuthenticated, userController.unfollowUser);

module.exports = router;
