const express = require("express");
const router = express.Router();
const postController = require("../controllers/postController");
const ensuredAuthenticated = require("../middleware/auth");
const uploadMiddleware = require("../middleware/upload_middleware");



router.get("/", ensuredAuthenticated, 
    uploadMiddleware.array("image", 3),
    postController.findAllPosts
);
router.get("/me", ensuredAuthenticated, 
    uploadMiddleware.array("image", 3),
    postController.findAllPostsByCurrentUser
);
router.get("/user/:id", ensuredAuthenticated, 
    uploadMiddleware.array("image", 3),
    postController.findAllPostsByUserId
);
router.post("/", ensuredAuthenticated, postController.createPost);
router.patch("/:id", ensuredAuthenticated, postController.updatePost);
router.patch("/like/:id", ensuredAuthenticated, postController.likePost);
router.patch("/unlike/:id", ensuredAuthenticated, postController.unlikePost);
router.delete("/:id", ensuredAuthenticated, postController.deletePost);


module.exports = router;
